<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct()
    {

    }

    public function index()
    {


        return redirect('admin/home');


    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function show()
    {

        return view('dashboard');
    }
}
