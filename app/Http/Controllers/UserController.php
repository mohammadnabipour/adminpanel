<?php

namespace App\Http\Controllers;
use App\Traits\SettingClient;
use App\User;
use Gnello\OpenFireRestAPI\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;


class UserController extends Controller
{
    use SettingClient;
    public function index()
    {
        $users = User::latest()->paginate(20);
        return view('user.index', compact('users'));
    }


    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {

        $client = $this->getClient();
        $response = $client->getUserModel()->createUser([
            "username" => $request->user_name,
            "name" => $request->name,
            "email" => $request->email,
            "password" => "p4ssword",
            "properties" => [
                [
                    "key" => "console.order",
                    "value" => "session-summary=0"
                ]
            ]
        ]);

        if ($response->getStatusCode() == 201) {
            $user = new User();
            $user->user_name = $request->user_name;
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->email = $request->email;
            $user->save();
            return Redirect::back()->with('message', 'user created succcessful');

        } else {
            echo "HTTP ERROR " . $response->getStatusCode();
        }


    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', compact('user'));
    }


    public function update(Request $request,$id)
    {
        $user = User::find($id);
        $client = $this->getClient();
        $response = $client->getUserModel()->updateUser($user->user_name,[
            "name" => $request->name,
            "email" => $request->email,
            "properties" => [
                [
                    "key" => "console.order",
                    "value" => "session-summary=0"
                ]
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return Redirect::back()->with('message', 'user updated succcessfuly');
        }else {
            echo "HTTP ERROR " . $response->getStatusCode();
        }

    }


    public function destroy($id)
    {
        $user = User::find($id);
        $client = $this->getClient();
        $response = $client->getUserModel()->deleteUser($user->user_name);

        if ($response->getStatusCode() == 200) {
            $user->delete();
            return Redirect::back()->with('message', 'user deleted succcessfuly');
        }else {
            echo "HTTP ERROR " . $response->getStatusCode();
        }


    }

}
